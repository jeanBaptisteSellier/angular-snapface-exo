import { Injectable } from '@angular/core';
import { FaceSnap } from '../models/face-snaps.model';

@Injectable({
    providedIn: 'root'
})

export class FaceSnapServices {
    faceSnaps: FaceSnap[] = [
        {
            id: 1,
          title: 'Archibald', 
          description: 'Mon ami depuis tout petit !', 
          imageUrl: 'https://cdn.pixabay.com/photo/2015/05/31/16/03/teddy-bear-792273_1280.jpg', 
          createdDate: new Date(), 
          snaps: 340,
          location: 'Paris'
        },
        {
            id: 2,
          title: 'Three Rock Mountains',
          description: 'Un endroit magnifique pour les randonnées.',
          imageUrl: 'https://hikesneardublin.com/wp-content/uploads/2019/07/20190728_105903.jpg',
          createdDate: new Date(),
          snaps: 0,
          location: 'la montagne'
        },
        {
            id: 3,
          title: 'The Great Wall of China',
          description: 'Un endroit magnifique pour les randonnées.',
          imageUrl: 'https://static.nationalgeographic.fr/files/styles/image_3200/public/8cfb252101c546c0b9897b8b794e1304.jpeg.webp?w=1450&h=816',
          createdDate: new Date(),
          snaps: 0
        }
      ];

    getAllFaceSnaps(): FaceSnap[] {
    return this.faceSnaps;
    }

    getFaceSnapById(faceSnapId: number): FaceSnap {
        const faceSnap = this.faceSnaps.find(faceSnap => faceSnap.id === faceSnapId);
        if(!faceSnap) {
          throw new Error('FaceSnap not found');
        } else {
          return faceSnap;
        }
      }

    snapFaceSnapById(faceSnapId: number, snapType: 'snap' | 'unSnap'): void {
        const faceSnap = this.getFaceSnapById(faceSnapId);
        snapType ==='snap'? faceSnap.snaps++ : faceSnap.snaps--;
    }
    
}
import { Component } from '@angular/core';
import { FaceSnap } from '../models/face-snaps.model';
import { FaceSnapServices } from '../services/face-snaps-services';

@Component({
  selector: 'app-face-snap-list',
  templateUrl: './face-snap-list.component.html',
  styleUrl: './face-snap-list.component.scss'
})
export class FaceSnapListComponent {
  
  faceSnaps!: FaceSnap[];

  constructor(private faceSnapServices: FaceSnapServices) { 
  }

  ngOnInit() {
    this.faceSnaps = this.faceSnapServices.getAllFaceSnaps();
  }

  

}

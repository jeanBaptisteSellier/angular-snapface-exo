import { Component, OnInit} from '@angular/core';
import { FaceSnap } from '../models/face-snaps.model';
import { FaceSnapServices } from '../services/face-snaps-services';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'single-face-snap',
  templateUrl: './single-face-snap.component.html',
  styleUrl: './single-face-snap.component.scss'
})
export class SingleFaceSnapComponent implements OnInit {
  faceSnap!: FaceSnap;
  buttonText!: string;

constructor(private faceSnapServices: FaceSnapServices,
            private route: ActivatedRoute) {}

ngOnInit() {
this.buttonText = 'Oh Snap!';

  const faceSnapId = Number(this.route.snapshot.paramMap.get('id'));
  if (faceSnapId !== null && !isNaN(faceSnapId)) {
  this.faceSnap = this.faceSnapServices.getFaceSnapById(faceSnapId);
  } else {
  throw new Error('No FaceSnap found with this id');
  }
}

  onAddSnap(){
    if (this.buttonText === 'Oh Snap!'){
      this.faceSnapServices.snapFaceSnapById(this.faceSnap.id, 'snap');
      this.buttonText = 'oops, unSnap!';
    } else {
      this.faceSnapServices.snapFaceSnapById(this.faceSnap.id, 'unSnap');
      this.buttonText = 'Oh Snap!';
    }
    
  }

}

import { Component, OnInit, Input } from '@angular/core';
import { FaceSnap } from '../models/face-snaps.model';
import { FaceSnapServices } from '../services/face-snaps-services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-face-snap',
  templateUrl: './face-snap.component.html',
  styleUrl: './face-snap.component.scss'
})
export class FaceSnapComponent implements OnInit {
  @Input() faceSnap!: FaceSnap;
  buttonText!: string;


constructor(private faceSnapServices: FaceSnapServices,
            private router: Router) {}

  ngOnInit(){
    this.buttonText = 'Oh Snap!';
  }

  onAddSnap(){
    if (this.buttonText === 'Oh Snap!'){
      this.faceSnapServices.snapFaceSnapById(this.faceSnap.id, 'snap');
      this.buttonText = 'oops, unSnap!';
    } else {
      this.faceSnapServices.snapFaceSnapById(this.faceSnap.id, 'unSnap');
      this.buttonText = 'Oh Snap!';
    }
    
  }

  onViewFaceSnapNap(){
    this.router.navigateByUrl(`/facesnaps/${this.faceSnap.id}`);
  }

}
